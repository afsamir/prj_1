const express = require('express');
const request = require('supertest');
const expect = require('chai').expect;

describe('Our server', function() {
    var app;
    var server;

    // Called once before any of the tests in this block begin.
    before(function(done) {
        app = require('../index');
        server = app.listen(function(err) {
            if (err) { return done(err); }
            done();
        });
    });

    it('should send back a JSON object with goodCall set to true', function(done) {
        request(app)
            .get('/we')
            .set('Content-Type', 'application/json')
            // .expect('Content-Type', '/json/')
            .expect(200, function(err, res) {
                if (err) { return done(err); }
                callStatus = res.body.msg;
                expect(callStatus).to.equal('yes');
                // Done
                done();
            });
    });


});
const express = require('express');
const PORT = 4000;

const app = express();
app.get('/we', (req, res, next) => {
    res.status(200).send({'msg':'yes'})
});

app.get('/you', (req, res, next) => {
    res.status(200).send({'msg':'yes'})
});


app.get('/they', (req, res, next) => {
    res.status(200).send({'msg':'yes'})
});

app.listen(PORT, () => {
    console.log(`Serving on ${PORT}`)
});


module.exports = app